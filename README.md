# MRSIFT
Map Reduce algorithm using SIFT features extraction. for the Advanced Operating System Course

# Note
This project is developed __ONLY__ for study purpose. All right to SIFT algorithm belongs to the creators

# Authors
*  didacusAbella https://github.com/didacusAbella
*  angeloPassaro https://github.com/angelopassaro - https://gitlab.com/AngeloPassaro
*  AddeusExMachina https://github.com/AddeusExMachina